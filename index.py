from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    #idSerie = 'SP68257'
    #url = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/{idSerie}/datos/{fechaIni}/{fechaFin}".format()
    #key = "50991bf30958046f15f5d87d900911190ad46c2f5cf06167cfc8b7dcbf5f69cb"
    return render_template('index.html')

if __name__ == '__main__':
    app.run('0.0.0.0', 5000, debug=True)