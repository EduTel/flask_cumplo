$(document).ready(function() {
    token = '0f8cb46ca996333073ac030187e63f5f236ff189c9107f44eb5f98da94acc059'
    console.log("===============ready")
    $(function() {
      $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        showButtonPanel: false,
        changeMonth: false,
        changeYear: false,
        /*showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        minDate: '+1D',
        maxDate: '+3M',*/
        inline: true
      });
      $.datepicker.setDefaults($.datepicker.regional['es']);
    });
    $.datepicker.regional['es'] = {
      closeText: 'Cerrar',
      prevText: '<Ant',
      nextText: 'Sig>',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
      dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
    };
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    firstDay = firstDay.toLocaleDateString("en-GB", options)
    lastDay = lastDay.toLocaleDateString("en-GB", options)
    console.log("firstDay",firstDay)
    console.log("lastDay",lastDay)
    from = $( ".datepicker.min" ).val(firstDay).datepicker({
    }).on( "change", function() {
        generate_chart(this.value,$( ".datepicker.max" ).val())
        get_tiie(this.value,$( ".datepicker.max" ).val())
        to.datepicker( "option", "minDate", getDate( this ) );
    }),
    to =$( ".datepicker.max" ).val(lastDay).datepicker({
    }).on( "change", function() {
        generate_chart($( ".datepicker.min" ).val(),this.value)
        get_tiie($( ".datepicker.min" ).val(),this.value)
        from.datepicker( "option", "maxDate", getDate( this ) );
    });
    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( 'dd/mm/yy' , element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    }
    function generate_chart(p_from='',p_to=''){
        console.log("======================generate_chart")
        console.log("p_from,",p_from)
        console.log("p_to",p_to)
        if(p_from!="" && p_to!=""){
            p_from = p_from.split("/").reverse().join("-")
            p_to = p_to.split("/").reverse().join("-")
            var ctx = document.getElementById('myChart').getContext('2d');
            var ctx2 = document.getElementById('myChart2').getContext('2d');
            url = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SP68257/datos/"+p_from+"/"+p_to+"?token="+token
            console.log("url", url)
            $.ajax({
                url: url,
                //dataType: "jsonp",
                success: function (data1) {
                    url2 = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF63528/datos/"+p_from+"/"+p_to+"?token="+token
                    data1_dato = data1.bmx.series[0].datos.map(function(num) {
                        return parseFloat(num.dato)
                    })
                    console.log(data1_dato)
                    $("#data_udis tr:nth-child(2) > td:nth-child(2)").text(Math.min.apply(null, data1_dato))
                    $("#data_udis tr:nth-child(3) > td:nth-child(2)").text(Math.max.apply(null, data1_dato))
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: data1.bmx.series[0].datos.map(function(num) {
                                return num.fecha
                            }),
                            datasets: [{
                                label: 'UDIS',
                                backgroundColor: '#0e6efd',
                                borderColor: '#0e6efd',
                                data: data1_dato,
                                fill: false,
                            }]
                        },
                        options: {
                            tooltips: {
                                enabled: true
                            }
                        }
                    });
                    console.log("url2",url2)
                    $.ajax({
                        url: url2,
                        //dataType: "jsonp",
                        success: function (data2) {
                            data2_dato = data2.bmx.series[0].datos.map(function(num) {
                                return parseFloat(num.dato)
                            })
                            console.log(data2_dato)
                            $("#data_dolar tr:nth-child(2) > td:nth-child(2)").text(Math.min.apply(null,data2_dato))
                            $("#data_dolar tr:nth-child(3) > td:nth-child(2)").text(Math.max.apply(null,data2_dato))
                            var myChart = new Chart(ctx2, {
                                type: 'line',
                                data: {
                                    labels: data2.bmx.series[0].datos.map(function(num) {
                                        return num.fecha
                                    }),
                                    datasets: [{
                                        label: 'DOLAR',
                                        backgroundColor: '#ff6384',
                                        borderColor: '#ff6384',
                                        data: data2_dato,
                                        fill: false,
                                    }]
                                },
                                options: {
                                    tooltips: {
                                        enabled: true
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    }
    async function get_tiie(p_from='',p_to=''){
        console.log("======================generate_chart")
        console.log("p_from,",p_from)
        console.log("p_to",p_to)
        if(p_from!="" && p_to!=""){
            p_from = p_from.split("/").reverse().join("-")
            p_to = p_to.split("/").reverse().join("-")
            var ctx3 = document.getElementById('myChart3').getContext('2d');
            series = ["SF331451","SF43783","SF43878","SF111916"]
            datas_tiie = []
            await fetch(`https://www.banxico.org.mx/SieAPIRest/service/v1/series/${series[0]}/datos/${p_from}/${p_to}?token=${token}`)
            .then(response => response.json())
            .then(data =>{
                data.bmx.series[0]["color"] = "#82BEDD"
                datas_tiie.push(data.bmx.series[0])
                return fetch(`https://www.banxico.org.mx/SieAPIRest/service/v1/series/${series[1]}/datos/${p_from}/${p_to}?token=${token}`)
            }).then(response => response.json())
            .then(data=>{
                data.bmx.series[0]["color"] = "#4D5357"
                datas_tiie.push(data.bmx.series[0])
                return fetch(`https://www.banxico.org.mx/SieAPIRest/service/v1/series/${series[2]}/datos/${p_from}/${p_to}?token=${token}`)
            }).then(response => response.json())
            .then(data=>{
                data.bmx.series[0]["color"] = "#A0A723"
                datas_tiie.push(data.bmx.series[0])
                return fetch(`https://www.banxico.org.mx/SieAPIRest/service/v1/series/${series[3]}/datos/${p_from}/${p_to}?token=${token}`)
            }).then(response => response.json())
            .then(data=>{
                data.bmx.series[0]["color"] = "#5C73F2"
                datas_tiie.push(data.bmx.series[0])
            })
            console.log("datas_tiie",datas_tiie)
            allfechas = datas_tiie.map(data=>{
                return data.datos.map(data=>{
                    return data.fecha
                })
            })
            console.log(allfechas)
            allfechas_Data=[]
            for (var i = 0; i<allfechas.length; i++) {
                allfechas[i].forEach((element, index, array)=>{
                    if(allfechas_Data.indexOf(element) === -1){
                        allfechas_Data.push(element)
                    }
                })
             }
            console.log(allfechas_Data)
            datasets_json = []
             console.log("================================")
            datas_tiie.forEach((element, index, array)=>{
                console.log(element)
                colors = []
                datos = allfechas_Data.map(data=>{
                    console.log(data)
                    colors.push(element.color)
                    data_return = null
                    element.datos.forEach((element, index, array)=>{
                        if(data.indexOf(element.fecha)!==-1){
                            console.log("encontrado")
                            console.log(element.dato)
                            data_return = parseFloat(element.dato)
                        }
                    })
                    console.log("data_return: ",data_return)
                    return data_return
                })
                console.log("datos", datos)
                colors[datos.indexOf(Math.max.apply(null,datos))] = "#f00"
                console.log("MAX",Math.max.apply(null,datos))
                console.log("MAX",datos.indexOf(Math.max.apply(null,datos)))
                datasets_json.push({
                    label: element.titulo,
                    backgroundColor: element.color,
                    borderColor: element.color,
                    pointBorderColor: colors,
                    data: datos,
                    fill: false,
                })
            })
            window.myChart = new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: allfechas_Data,
                    datasets: datasets_json
                },
                options: {
                    tooltips: {
                        enabled: true
                    }
                }
            });
            //window.myChart.data.datasets[0].pointBorderColor[0] = "#ff6384";
            //window.myChart.update();
        }
    }
    //https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF331451/datos/2021-01-01/2021-01-31?token=0f8cb46ca996333073ac030187e63f5f236ff189c9107f44eb5f98da94acc059
    //https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43783/datos/2021-01-01/2021-01-31?token=0f8cb46ca996333073ac030187e63f5f236ff189c9107f44eb5f98da94acc059
    //https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43878/datos/2021-01-01/2021-01-31?token=0f8cb46ca996333073ac030187e63f5f236ff189c9107f44eb5f98da94acc059
    //https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF111916/datos/2021-01-01/2021-01-31?token=0f8cb46ca996333073ac030187e63f5f236ff189c9107f44eb5f98da94acc059
    generate_chart($( ".datepicker.min" ).val(),$( ".datepicker.max" ).val())
    get_tiie($( ".datepicker.min" ).val(),$( ".datepicker.max" ).val())
});